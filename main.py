class MainClass:
    @staticmethod
    def main():
        print("Hello, World!")

if __name__ == "__main__":
    MainClass.main()
